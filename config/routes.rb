Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  root "welcome#index"
  post "/authenticate", action: :create, controller: :authentications
  post "/revoke", action: :revoke, controller: :authentications
  post "/refresh", action: :refresh, controller: :authentications
  post "/user", action: :create, controller: :users
  get "/users/:id", action: :show, controller: :users, as: :user_profile
  get "/user/me/password/:id", action: :password, controller: :users, as: :user_password
  post "/user/me/password", action: :change_password, controller: :users
  post "/users/reset_password", action: :reset_password, controller: :users
  put "/users/me", action: :update, controller: :users
  get "/user/my_widgets", action: :my_widgets, controller: :users
  get "/users/:id/widgets", action: :user_widget_index, controller: :users
  post "/widgets", action: :create, controller: :widgets
  put "/widgets/:id", action: :update, controller: :widgets
  get "/widgets", action: :index, controller: :widgets
  get "/visible_widgets", action: :visible, controller: :widgets
  delete "/widgets/:id", action: :destroy, controller: :widgets, as: :delete_widget
end
