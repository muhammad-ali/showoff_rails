class WidgetsController < ApplicationController
  before_action :refresh_user
  def create
    body = {}
    body['widget'] = params['widget']
    body['widget']['kind'] = body['widget']['kind'].present? ? 'visible' : 'hidden'
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/widgets"
    response = RestClient.post url, body, content_type: :json, authorization: current_user
    result = JSON.parse response
    redirect_to widgets_path
  end

  def update
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/widgets/#{params['id']}"
    response = RestClient.put url, params, content_type: :json, authorization: request.headers['Authorization']
    result = JSON.parse response
    render json: result
  end

  def index
    if params['search'].present?
      url = "https://showoff-rails-react-production.herokuapp.com/api/v1/widgets/visible?client_id=#{ENV['CLIENT_ID']}&client_secret=#{ENV['CLIENT_SECRET']}&term=#{params['search']}"
    else
      url = "https://showoff-rails-react-production.herokuapp.com/api/v1/widgets/visible?client_id=#{ENV['CLIENT_ID']}&client_secret=#{ENV['CLIENT_SECRET']}"
    end
    if current_user.present?
      response = RestClient.get url, content_type: :json, authorization: current_user
      response = JSON.parse response
      @widgets = response['data']['widgets']
    end
  end

  def show

  end

  def visible
    url = "https://showoff-rails-react-production.herokuapp.com/api/widgets/visible?client_id=#{ENV['client_id']}&client_secret=#{ENV['client_secret']}"
    response = RestClient.get url, content_type: :json, authorization: request.headers['Authorization']
    result = JSON.parse response
    render json: result
  end

  def destroy
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/widgets/#{params['id']}"
    response = RestClient.delete url, content_type: :json, authorization: current_user
    response = JSON.parse response
    if response['message'] == 'success'
      redirect_to :back
    else
      flash['error'] = "You can not delete this widget"
      redirect_to :back
    end
  end
end
