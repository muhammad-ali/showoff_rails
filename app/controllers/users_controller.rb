class UsersController < ApplicationController
  def create
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users"
    body = {'client_id' => ENV['CLIENT_ID'], 'client_secret' => ENV['CLIENT_SECRET']}
    body['user'] = params['user']
    response = RestClient.post(url, body.to_json, { content_type: :json, accept: :json })
    result = JSON.parse response
    session['current_user'] = result['data']
    redirect_to widgets_path
  end

  def show
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users/#{params['id']}"
    response = RestClient.get url, content_type: :json, authorization: current_user
    response = JSON.parse response
    if response['message'] == 'Success'
      @user = response['data']['user']
    else
      flash['error'] = "You can view profile"
      redirect_to :back
    end
  end

  def change_password
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users/me/password"
    response = RestClient.post url, params, content_type: :json, authorization: request.headers['Authorization']
    result = JSON.parse response
    render json: result
  end

  def password
    if current_user_id.to_s == params['id'].to_s
    else
      flash['error'] = "You can change password"
      redirect_to :back
    end
  end

  def change_password
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users/me/password"
    body = {}
    body['user'] = params['user']
    response = RestClient.post url, body, content_type: :json, authorization: current_user
    response = JSON.parse response
    session['current_user'] = response['data']
    redirect_to widgets_path
  end

  def reset_password
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users/reset_password"
    response = RestClient.post url, params, content_type: :json
    result = JSON.parse response
    render json: result
  end

  def update
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users/me"
    response = RestClient.put url, params, content_type: :json, authorization: request.headers['Authorization']
    result = JSON.parse response
    render json: result
  end

  def my_widgets
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users/me/widgets?client_id=#{ENV['CLIENT_ID']}&client_secret=#{ENV['CLIENT_SECRET']}"
    response = RestClient.get url, content_type: :json, authorization: current_user
    response = JSON.parse response
    @widgets = response['data']['widgets']
  end

  def user_widget_index
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users/#{params['id']}/widgets?client_id=#{params['client_id']}&client_secret=#{params['client_secret']}&term=#{params['visible']}"
    response = RestClient.get url, content_type: :json, authorization: request.headers['Authorization']
    result = JSON.parse response
    render json: result
  end
end
# {
#     "code": 0,
#     "message": "Success",
#     "data": {
#         "user": {
#             "id": 198,
#             "name": "A User",
#             "images": {
#                 "small_url": "https://showoff-rails-react-api-production.s3.amazonaws.com/users/images/missing/missing.jpg",
#                 "medium_url": "https://showoff-rails-react-api-production.s3.amazonaws.com/users/images/missing/missing.jpg",
#                 "large_url": "https://showoff-rails-react-api-production.s3.amazonaws.com/users/images/missing/missing.jpg",
#                 "original_url": "https://showoff-rails-react-api-production.s3.amazonaws.com/users/images/missing/missing.jpg"
#             },
#             "first_name": "A",
#             "last_name": "User",
#             "date_of_birth": null,
#             "email": "nu.muhammad.ali@gmail.com",
#             "active": true
#         },
#         "token": {
        #     "access_token": "981b8ba1b63765ccd0ca64a7121b397b44255e727a7f730b79cb0fed16eda31b",
        #     "token_type": "Bearer",
        #     "expires_in": 2592000,
        #     "refresh_token": "cd4825fcbce961e2466abb0a7e95118491217d72acfb89180facddb6fde9459c",
        #     "scope": "basic",
        #     "created_at": 1549693819
        # }
#     }
# }
