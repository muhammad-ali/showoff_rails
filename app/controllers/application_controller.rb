class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  respond_to :html, :json

  require 'rest-client'

  helper_method [:current_user, :get_current_user,
    :refresh_user, :current_user_id, :save_user_in_session]

  def current_user
    user_token = session['current_user']
    if user_token.present? && user_token['token'].present? && user_token['token']['access_token'].present?
      "Bearer #{user_token['token']['access_token']}"
    else
      ''
    end
  end

  def get_current_user
    if current_user.present?
      session['current_user']['user']
    else
      ''
    end
  end

  def refresh_user
    if current_user.present?
      user_token = session['current_user']['token']
      body = {
        "grant_type": "refresh_token",
        "refresh_token": user_token['refresh_token'],
        "client_id": ENV['CLIENT_ID'],
        "client_secret": ENV['CLIENT_SECRET']
      }
      url = "https://showoff-rails-react-production.herokuapp.com/oauth/token"
      response = RestClient.post url, body, content_type: :json, authorization: current_user
      response = JSON.parse response
      session['current_user']['token'] = response['data']['token']
    else
      flash['error'] = "You must be logged in to access this section"
      redirect_to root_path
    end
  end

  def current_user_id
    if current_user.present?
      user = get_current_user
      user['id']
    else
      ''
    end
  end

  def save_user_in_session
    url = "https://showoff-rails-react-production.herokuapp.com/api/v1/users/me"
    response = RestClient.get url, content_type: :json, authorization: current_user
    response = JSON.parse response
    session['current_user']['user'] = response['data']['user']
  end
end
