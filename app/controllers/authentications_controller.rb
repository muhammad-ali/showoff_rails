class AuthenticationsController < ApplicationController
  def create
    url = "https://showoff-rails-react-production.herokuapp.com/oauth/token"
    body = {}
    body['username'] = params[:email]
    body['password'] = params[:password]
    body['grant_type'] = 'password'
    body['client_id'] = ENV['CLIENT_ID']
    body['client_secret'] = ENV['CLIENT_SECRET']
    response = RestClient.post url, body, content_type: :json
    response = JSON.parse response
    if response['message'] == 'Success'
      session['current_user'] = response['data']
      save_user_in_session
      redirect_to widgets_path
    else
      flash['error'] = "You can not login"
      redirect_to :back
    end
  end

  def revoke
    url = "https://showoff-rails-react-production.herokuapp.com/oauth/revoke"
    if current_user.present?
      session['current_user'] = ''
      redirect_to root_path
    else
      flash['error'] = "You can not logout"
      redirect_to :back
    end
  end

  def refresh
    url = "https://showoff-rails-react-production.herokuapp.com/oauth/token"
    response = RestClient.post url, params, content_type: :json, authorization: "Bearer #{params['token']}"
    result = JSON.parse response
    render json: result
  end
end
